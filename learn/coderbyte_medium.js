function PrimeTime(num) {
	for (var i = num - 1; (i > 1) && (i < num); i--) {
		if (num % i === 0) {
			return false;
		}
	}
	return true;
}